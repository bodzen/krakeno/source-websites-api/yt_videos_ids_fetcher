# Youtube Videos Ids Fetcher

```diff
- This project is not used anymore, and probably won't be maintain.
```

![api_response](https://gitlab.com/bodzen/images-stocks/raw/master/spaces/spaceships/speeder_01.jpg)

This module fetch all the videos' ids from a Youtube playlist and return it as a list.
It uses the Google API. See its doc here:
https://developers.google.com/youtube/v3/docs/playlistItems/list

## Usage
```
>>> from yt_videos_ids_fetcher import youtube_videos_ids_fetcher as fetcher
>>> inst = fetcher(PLAYLIST_ID)
>>> print(inst.get_videos_ids_list)
```

## Installation

### requirements.txt && pip
```
-e git+ssh://git@gitlab.com/krakeno/source-websites-api/yt_videos_ids_fetcher.git#egg=yt_videos_ids_fetcher
```
