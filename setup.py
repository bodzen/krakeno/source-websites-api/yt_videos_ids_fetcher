#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup  # pyre-ignore
from pip._internal.req import parse_requirements

def load_requirements():
    required_lib_list = parse_requirements('requirements.txt', session="test")
    return [str(lib.req) for lib in required_lib_list]

setup(
    name='yt_videos_ids_fetcher',
    version='1.0',
    author='dh4rm4',
    description="Fetch all the videos' ids from a Youtube playlist.",
    install_requires=load_requirements(),
    classifiers=[
        "Programming Language :: Python 3.7.2",
        "Development Status :: Done",
        "Language :: English",
        "Operating System :: Debian based",
    ],
)
