import unittest
from typing import NoReturn
from yt_videos_ids_fetcher import youtube_videos_ids_fetcher as fetcher


PLAYLIST_ID = 'PLLP4m0O8UhixrB4uVpeSfAb6W_8UaQcyx'


class results_test(unittest.TestCase):
    inst_obj: fetcher = None

    def setUp(self) -> None:
        self.inst_obj = fetcher(PLAYLIST_ID)

    def test_returned_obj_type(self) -> NoReturn:
        returned_content = self.inst_obj.get_videos_ids_list()
        self.assertTrue(isinstance(returned_content, list))

    def test_returned_obj_content(self) -> NoReturn:
        valid_content = ['-Smm8RH8DgU', 'cmhEg-nDGJA', 'G_Mwr1-dWJ4',
                         'pTuyfQ5CR4Q', 'P4mY4qmuJas', 'pPUey-qBNAI',
                         'aMkNwBp_Ows', 'ooisvdBHuP8']
        returned_content = self.inst_obj.get_videos_ids_list()
        self.assertEqual(returned_content, valid_content)
