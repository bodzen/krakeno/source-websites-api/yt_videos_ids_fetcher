import unittest
import logging
from typing import NoReturn
from yt_videos_ids_fetcher import youtube_videos_ids_fetcher as fetcher
from googleapiclient.discovery import Resource  # pyre-ignore


logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.ERROR)


PLAYLIST_ID = 'PLLP4m0O8UhixrB4uVpeSfAb6W_8UaQcyx'


class object_initialisation_tests(unittest.TestCase):
    inst_obj: fetcher = None

    def setUp(self) -> None:
        self.inst_obj = fetcher(PLAYLIST_ID)
        self.inst_obj.init_google_api()

    def test_api_options_init(self) -> NoReturn:
        valid_api_options = {'part': 'contentDetails',
                             'maxResults': 50,
                             'playlistId': PLAYLIST_ID}
        self.assertEqual(valid_api_options, self.inst_obj.api_options)

    def test_client_api_init(self) -> NoReturn:
        self.assertTrue(isinstance(self.inst_obj.client_api, Resource))
