import unittest
from typing import NoReturn
from yt_videos_ids_fetcher import youtube_videos_ids_fetcher as fetcher


class api_response_unittest_mixin:
    inst_obj: fetcher = None
    error_msg: str = None
    playlist_id: str = None

    def setUp(self) -> None:
        self.inst_obj = fetcher(self.playlist_id)
        self.inst_obj.init_google_api()
        self.inst_obj.get_one_page_of_videos_infos()

    def test_if_error_is_true(self) -> NoReturn:
        err_state = self.inst_obj.call_state['error']
        self.assertEqual(err_state, True)  # pyre-ignore

    def test_error_msg(self) -> NoReturn:
        err_msg = self.inst_obj.call_state['error_msg']
        self.assertEqual(err_msg, self.error_msg)  # pyre-ignore


class unknow_playlist_test(api_response_unittest_mixin,
                           unittest.TestCase):
    playlist_id: str = 'UNKOWN_PLAYLIST_ID'
    error_msg: str = 'YT_API_PLAYLIST_NOT_FOUND'
