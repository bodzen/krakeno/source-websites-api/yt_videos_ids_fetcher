#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    This module fetch all the videos' ids from a
    Youtube playlist and return it as a list

    To import the main class:
    >>> from yt_videos_ids_fetcher import youtube_videos_ids_fetcher
    >>> help(youtube_videos_ids_fetcher)
"""

from yt_videos_ids_fetcher.fetcher import youtube_videos_ids_fetcher
