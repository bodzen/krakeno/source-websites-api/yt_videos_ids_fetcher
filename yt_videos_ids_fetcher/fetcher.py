#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Class used to fetch youtube videos ids

    Usage:
    >>> from yt_videos_ids_fetcher import youtube_videos_ids_fetcher as fetcher
    >>> inst = fetcher(PLAYLIST_ID)
    >>> print(inst.get_videos_ids_list)
"""

from typing import Union, Dict, NoReturn
import logging
from googleapiclient.discovery import build  # pyre-ignore
from googleapiclient.errors import HttpError

__all__ = ['youtube_videos_ids_fetcher']

# Silence google package import error (pck link to oauth2)
logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.ERROR)

# Will be manage in db see ticket #1
API_SERVICE_NAME: str = 'youtube'
API_VERSION: str = 'v3'
API_KEY: str = 'AIzaSyD84iHyie-MuetegQGosWAR7L-SyvkudTA'


class youtube_videos_ids_fetcher:  # pylint: disable=E1101
    """
        Uses Youtube Data API to fetch the videos ids from the given playlist
        Doc: https://developers.google.com/youtube/v3/docs/playlistItems/list
    """
    total_nb_items: int = 1
    nb_item_done: int = 0
    videos_ids: list = []
    call_state: Dict[str, Union[bool, str]] = {'error': False,
                                               'error_msg': None}
    api_options: Dict[str, Union[int, str]] = None
    client_api = None

    def __init__(self, playlist_id: str):
        self.playlist_id = playlist_id
        self.videos_ids = []

    def get_videos_ids_list(self) -> list:
        """
            Build and return a list of videos ids
            from the given playlist
        """
        self.init_google_api()
        while self.nb_item_done < self.total_nb_items:
            response = self.get_one_page_of_videos_infos()
            if self.call_state['error'] is True:
                return []

            self.store_videos_ids_from_response(response)
            self.update_fetching_progression(response)
            if self.nb_item_done < self.total_nb_items:
                self.add_next_page_token_to_api_options(response)
        return self.videos_ids

    def init_google_api(self) -> NoReturn:
        self.build_api_link()
        self.api_options = {'part': 'contentDetails',
                            'maxResults': 50,
                            'playlistId': self.playlist_id}

    def build_api_link(self) -> NoReturn:
        """
            Build an API link with google servers
        """
        self.client_api = build(API_SERVICE_NAME,
                                API_VERSION,
                                developerKey=API_KEY)

    def store_videos_ids_from_response(self, response):
        """
            Extract all the videos Ids from the API response
        """
        for video_infos in response['items']:
            self.videos_ids.append(video_infos['contentDetails']['videoId'])

    def get_one_page_of_videos_infos(self):
        """
            Use the Youtube Data API to fecth one page of infos
            on the videos from the given playlist
        """
        try:
            api_call = self.client_api.playlistItems()
            response = api_call.list(**self.api_options).execute()
        except HttpError as err:
            self.call_state['error'] = True
            if err.resp.status == 404:
                self.call_state['error_msg'] = 'YT_API_PLAYLIST_NOT_FOUND'
            elif err.resp.status == 403:
                self.call_state['error_msg'] = 'YT_API_PLAYLIST_NO_ACCESSIBLE'
            else:
                self.call_state['error_msg'] = 'YT_API_UNKNOW_ERROR'
            return None
        except Exception:  # pylint: disable=W0703
            self.call_state['error'] = True
            self.call_state['error_msg'] = 'YT_API_UNKNOWN_ERROR'
            return None
        return response

    def update_fetching_progression(self, response) -> NoReturn:
        """
            Based on response from Youtube API
            Update current progression of infos fetching
        """
        if self.total_nb_items == 1:
            self.total_nb_items = response['pageInfo']['totalResults']
        self.nb_item_done += response['pageInfo']['resultsPerPage']

    def add_next_page_token_to_api_options(self, response) -> NoReturn:
        self.api_options['pageToken'] = response['nextPageToken']
